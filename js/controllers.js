testing.controllers.controller('AppCtrl', ['$scope','$rootScope', function($scope,$rootScope){

	$rootScope.users = [];
}]);

testing.controllers.controller('MainCtrl', ['$scope','$rootScope', function($scope, $rootScope){
	var User = function(name){
		this.name = name || "name";
		this.age = Math.round(Math.random()*100);
	};

	$scope.registerUser = new User();

	$scope.submit = function(user){
		$rootScope.users.unshift(user);
		$scope.registerUser = new User("name"+$rootScope.users.length);//Watch out! 
	};

	$scope.remove = function(){
		alert("Good Luck.");
	};
}]);

testing.controllers.controller('UserCtrl', ['$scope','$rootScope','$routeParams','$location', function($scope, $rootScope, $routeParams, $location){
	var index = $routeParams.index;
	$scope.user = angular.copy($rootScope.users[index]||{});

	$scope.submit = function(){
		$rootScope.users[index] = $scope.user;
		$location.path('/users');
	};

	$scope.cancel = function(){
		$location.path('/users');
	};
}]);