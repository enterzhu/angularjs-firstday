var testing = angular.module('testing', ['testing.controllers','ngRoute']);
testing.controllers = angular.module('testing.controllers', []);

// ----------------- Add Router ----------------
testing.config(['$routeProvider',function ($routeProvider) {

    $routeProvider
        .when('/users/:index', {
            controller: 'UserCtrl',
            templateUrl: '/html/user.html'
        }).when('/users', {
            controller: 'MainCtrl',
            templateUrl: '/html/main.html'
        });
    $routeProvider.otherwise({
            redirectTo: '/users'
        });
}]);